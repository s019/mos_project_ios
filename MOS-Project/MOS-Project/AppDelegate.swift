//
//  AppDelegate.swift
//  MOS-Project
//
//  Created by Gabriel Schützeneder on 14.11.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//

import UIKit
import CoreData
import HealthKit
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private let manager = CLLocationManager()
    let cdManager: CoreDataManager = CoreDataManager.sharedInstance
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if UserDefaults.standard.object(forKey: "settings") == nil {
            print("value not set")
            UserDefaults.standard.set(false, forKey: "settings")
            UserDefaults.standard.synchronize()
        }
        
        if UserDefaults.standard.object(forKey: "setData") == nil {
            UserDefaults.standard.set(true, forKey: "setData")
            UserDefaults.standard.synchronize()
            
            cdManager.update { (context) in
                var c = DateComponents()
                c.year = 2017
                c.month = 12
                c.day = 2
                c.hour = 12
                c.minute = 0
                var date = Calendar(identifier: .gregorian).date(from: c)
                var session: Session = Session(context: context)
                session.start = date! as NSDate
                c.hour = 13
                date = Calendar(identifier: .gregorian).date(from: c)
                session.end = date! as NSDate
                session.trimp = 33
                session.kcal = 1000
                session.fatigue = 33
                session.fitness = 1000
                session.performance = 934
                
                c.year = 2017
                c.month = 12
                c.day = 3
                c.hour = 12
                c.minute = 0
                date = Calendar(identifier: .gregorian).date(from: c)
                session = Session(context: context)
                session.start = date! as NSDate
                c.hour = 13
                date = Calendar(identifier: .gregorian).date(from: c)
                session.end = date! as NSDate
                session.trimp = 107
                session.kcal = 1082
                session.fatigue = 137
                session.fitness = 1082
                session.performance = 808
                
                c.year = 2017
                c.month = 12
                c.day = 4
                c.hour = 12
                c.minute = 0
                date = Calendar(identifier: .gregorian).date(from: c)
                session = Session(context: context)
                session.start = date! as NSDate
                c.hour = 13
                date = Calendar(identifier: .gregorian).date(from: c)
                session.end = date! as NSDate
                session.trimp = 107
                session.kcal = 1163
                session.fatigue = 232
                session.fitness = 1163
                session.performance = 698
            }
            print("fake session successfully set!")
        }
        
        requestAccessToHealthKit()
        requestLocationServices()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    private func requestAccessToHealthKit() {
        let healthStore = HKHealthStore()
        
        // for future implementations…
        // HKObjectType.workoutType()
        // HKSeriesType.workoutRoute()
        // HKObjectType.quantityType(forIdentifier: .activeEnergyBurned)!
        // HKObjectType.quantityType(forIdentifier: .distanceWalkingRunning)!
        
        let allTypes = Set([HKObjectType.quantityType(forIdentifier: .heartRate)!])
        
        healthStore.requestAuthorization(toShare: allTypes, read: allTypes) { (success, error) in
            if !success {
                print(error?.localizedDescription ?? "")
            }
        }
    }
    
    private func requestLocationServices() {
        if CLLocationManager.authorizationStatus() == .notDetermined {
            manager.requestAlwaysAuthorization()
        }
    }
}
