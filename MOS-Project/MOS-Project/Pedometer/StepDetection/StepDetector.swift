//
//  StepDetector.swift
//  MOS-Project
//
//  Created by Gabriel Schützeneder on 16.11.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//

import Foundation

protocol DetectionDelegate: class {
    func step(timeNs: Int64)
}

class StepDetector {
    
    weak var detectionDelegate: DetectionDelegate?
    
    let ACCEL_RING_SIZE       = 50
    let VEL_RING_SIZE         = 10
    let STEP_THRESHOLD: Float = 40
    let STEP_DELAY_NS         = 250000000
    var accelRingCounter      = 0
    
    let sensorFilter = SensorFilter.sharedInstance
    
    var accelRingX: [Float] = [Float]()
    var accelRingY: [Float] = [Float]()
    var accelRingZ: [Float] = [Float]()
    
    var velRingCounter   = 0
    var velRing: [Float] = [Float]()
    
    var lastStepTimeNs: Int64      = 0
    var oldVelocityEstimate: Float = 0
    
    init() {
        accelRingX = Array(repeating: Float(0.0), count: ACCEL_RING_SIZE)
        accelRingY = Array(repeating: Float(0.0), count: ACCEL_RING_SIZE)
        accelRingZ = Array(repeating: Float(0.0), count: ACCEL_RING_SIZE)
        
        velRing = Array(repeating: Float(0.0), count: VEL_RING_SIZE)
    }
    
    func updateAcceleration(timeNs: Int64, x: Float, y: Float, z: Float) {
        // get newest values
        var currentAccel = [Float]()
        currentAccel.append(x)
        currentAccel.append(y)
        currentAccel.append(z)
        
        // First step is to update our guess of where the global z vector is.
        accelRingCounter += 1
        accelRingX[accelRingCounter % ACCEL_RING_SIZE] = currentAccel[0]
        accelRingY[accelRingCounter % ACCEL_RING_SIZE] = currentAccel[1]
        accelRingZ[accelRingCounter % ACCEL_RING_SIZE] = currentAccel[2]

        var worldZ = [Float]()
        worldZ.append(sensorFilter.sum(array: accelRingX) / Float(min(accelRingCounter, ACCEL_RING_SIZE)))
        worldZ.append(sensorFilter.sum(array: accelRingY) / Float(min(accelRingCounter, ACCEL_RING_SIZE)))
        worldZ.append(sensorFilter.sum(array: accelRingZ) / Float(min(accelRingCounter, ACCEL_RING_SIZE)))
        
        let normalization_factor: Float = sensorFilter.norm(array: worldZ)
        worldZ[0] = worldZ[0] / normalization_factor
        worldZ[1] = worldZ[1] / normalization_factor
        worldZ[2] = worldZ[2] / normalization_factor
        
        let currentZ: Float = sensorFilter.dot(a: worldZ, b: currentAccel) - normalization_factor
        velRingCounter += 1
        velRing[velRingCounter % VEL_RING_SIZE] = currentZ
        
        let velocityEstimate: Float = sensorFilter.sum(array: velRing)
        
        if velocityEstimate > STEP_THRESHOLD && oldVelocityEstimate <= STEP_THRESHOLD && (timeNs - lastStepTimeNs > STEP_DELAY_NS) {
            self.detectionDelegate?.step(timeNs: timeNs)
            lastStepTimeNs = timeNs
        }
        oldVelocityEstimate = velocityEstimate
    }
}
