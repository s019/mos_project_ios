//
//  MotionManager.swift
//  MOS-Project
//
//  Created by Manuel Leibetseder on 21.11.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//

import Foundation
import CoreMotion

protocol StepDelegate: class {
    func update(_ stepCount: Int)
}

final class StepManager {
    
    private var motionManager: CMMotionManager!
    private let motionQueue: OperationQueue!
    private let cdManager: CoreDataManager!
    private var stepDetector: StepDetector!
    private var apiHandler: APIHandler?
    private var steps: Int
    weak var delegate: StepDelegate?
    
    convenience init(inject apiHandler: APIHandler) {
        self.init()
        self.apiHandler = apiHandler
    }
    
    init() {
        self.motionQueue   = OperationQueue()
        self.cdManager     = CoreDataManager.sharedInstance
        self.motionManager = CMMotionManager()
        self.stepDetector  = StepDetector()
        self.steps         = 0
        
        self.stepDetector.detectionDelegate = self
        self.motionManager.startAccelerometerUpdates()
        self.motionManager.accelerometerUpdateInterval = 0.01
        self.motionManager.startAccelerometerUpdates(to: motionQueue, withHandler: self.onAccelerometerUpdate)
    }

    private func onAccelerometerUpdate(data: CMAccelerometerData?, error: Error?) {
        
        if error == nil, let d = data {
            self.stepDetector.updateAcceleration(timeNs: Int64(d.timestamp * 1000000000), // Nanoseconds!
                x: Float(d.acceleration.x * 9.81),
                y: Float(d.acceleration.y * 9.81),
                z: Float(d.acceleration.z * 9.81)
            )
        }
    }
    
    func saveStep(with id: String, and date: Date) {
        self.cdManager.update { context in
            
            let step: Step         = Step(context: context)
            let location: Location = Location(context: context)
            
            step.id            = id
            step.timestamp     = date as NSDate
            location.latitude  = 0
            location.longitude = 0
            step.location      = location
        }
    }
}

extension StepManager: DetectionDelegate {
    
    func step(timeNs: Int64) {
        
        self.apiHandler?.post(date: Date()) { [weak self] id, date in
            if let id = id {
                self?.saveStep(with: id, and: date)
            }
        }
        self.steps += 1
        self.delegate?.update(self.steps)
    }
}
