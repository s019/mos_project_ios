//
//  SensorFilter.swift
//  MOS-Project
//
//  Created by Gabriel Schützeneder on 16.11.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//

import Foundation

class SensorFilter {
    
    static let sharedInstance = SensorFilter()
    
    init() {}
    
    func sum(array: [Float]) -> Float {
        var retval: Float = 0
        for item in array {
            retval += item
        }
        return retval
    }
    
    func cross(arrayA: [Float], arrayB: [Float]) -> [Float] {
        var retArray = [Float]()
        retArray.append(arrayA[1] * arrayB[2] - arrayA[2] * arrayB[1])
        retArray.append(arrayA[2] * arrayB[0] - arrayA[0] * arrayB[2])
        retArray.append(arrayA[0] * arrayB[1] - arrayA[1] * arrayB[0])
        return retArray
    }
    
    func norm(array: [Float]) -> Float {
        var retval: Float = 0
        
        for item in array {
            retval += item * item
        }
        return sqrt(retval) as Float
    }
    
    func dot(a: [Float], b: [Float]) -> Float {
        return a[0] * b[0] + a[1] * b[1] + a[2] * b[2]
    }
    
    func normalize(array: [Float]) -> [Float] {
        var retval = [Float]()
        let norm = self.norm(array: array)
        for item in array {
            retval.append(item / norm)
        }
        return retval
    }
}
