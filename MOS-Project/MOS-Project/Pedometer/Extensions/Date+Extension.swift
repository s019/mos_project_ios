//
//  DateComponent+Extension.swift
//  MOS-Project
//
//  Created by Gabriel Schützeneder on 20.11.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//

import Foundation

extension Int {
    static var daysInCurrentMonth: Int {
        let calendar       = Calendar.current
        let dateComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: Date())
        
        let date  = calendar.date(from: dateComponents)!
        let range = calendar.range(of: .day, in: .month, for: date)!
        return range.count
    }
}

extension Date {
    
    static var startOfWeek: Date {

        return Calendar.current.date(
            from: Calendar.current.dateComponents(
                [.yearForWeekOfYear, .weekOfYear],
                from: Date()
            )
        )!
    }

    static var endOfWeek: Date {
    
        return Calendar.current.date(
            byAdding: .day, value: 7,
            to: self.startOfWeek
        )!
    }
    
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
}
