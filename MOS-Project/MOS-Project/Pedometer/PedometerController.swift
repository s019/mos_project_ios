//
//  PedometerViewController.swift
//  Project
//
//  Created by Gabriel Schützeneder on 13.11.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//

import UIKit

import CoreLocation
import Charts

class PedometerController: UIViewController {
    
    @IBOutlet weak var pedometerLabel: UILabel!
    @IBOutlet weak var barChartView: BarChartView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var steps: Int = 0
    
    // get accelerometer data
    var stepManager: StepManager!

    // get location data
    var locationManager: CLLocationManager!
    var chartHandler: ChartHandler!
    var apiHandler: APIHandler!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.apiHandler = APIHandler()
        self.stepManager          = StepManager(inject: self.apiHandler)
        self.stepManager.delegate = self
        
        self.chartHandler          = ChartHandler()
        self.chartHandler.delegate = self
        
        self.pedometerLabel.textAlignment = .center
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()

        self.chartHandler.presentToday()
    }
    
    @IBAction func switchedChartView(_ sender: Any) {
        
        switch StepView(rawValue: self.segmentedControl.selectedSegmentIndex)! {
        case .today:
            self.chartHandler.presentToday()
        case .week:
            self.chartHandler.presentWeek()
        case .month:
            self.chartHandler.presentMonth()
        case .year:
            self.chartHandler.presentYear()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
      /*  var counter = 0
        for hour in stepsDay {
            print("\(counter + 1): \(hour)")
            counter += 1
        }*/
    }
    
    func setChart(amount: Int, values: [Int]) {
        
    }
    
    /*func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
     if status == .authorizedWhenInUse {
     if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
     if CLLocationManager.isRangingAvailable() {
     locationManager.desiredAccuracy = kCLLocationAccuracyBest
     locationManager.requestLocation()
     //print("\(locationManager.location?.coordinate.latitude);\(locationManager.location?.coordinate.longitude)")
     }
     }
     }
     }*/
}

extension PedometerController: StepDelegate {
    
    func update(_ stepCount: Int) {
        DispatchQueue.main.async { [weak self] in
            self?.pedometerLabel.text = "Steps to go: \(self!.steps - stepCount)"
        }
    }
}

extension PedometerController: ChartDelegate {
    func presentData(by amount: Int, and values: [Int]) {
        barChartView.noDataText = "No data available"
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<amount {
            let dataEntry = BarChartDataEntry(x: Double(i), y: Double(values[i]))
            dataEntries.append(dataEntry)
        }
        var labelString: String = ""
        if segmentedControl.selectedSegmentIndex == 0 {
            self.pedometerLabel.text = "Steps to go: \(7000 - values.reduce(0, +))"
            self.steps = 7000 - values.reduce(0, +)
            labelString = "Amount of steps per hour"
            barChartView.xAxis.granularity = 3
            barChartView.xAxis.valueFormatter = XAxisValueFormatter(chart: barChartView, selected: 0) // change X-Axis description
        } else if segmentedControl.selectedSegmentIndex == 1 {
            labelString = "Amount of steps per weekday"
            barChartView.xAxis.granularity = 1
            barChartView.xAxis.valueFormatter = XAxisValueFormatter(chart: barChartView, selected: 1) // change X-Axis description
        } else if segmentedControl.selectedSegmentIndex == 2 {
            labelString = "Amount of steps per day"
            barChartView.xAxis.valueFormatter = nil
            barChartView.xAxis.granularity = 3
        } else if segmentedControl.selectedSegmentIndex == 3 {
            barChartView.xAxis.valueFormatter = XAxisValueFormatter(chart: barChartView, selected: 3) // change X-Axis description
            labelString = "Amount of steps per month"
            barChartView.xAxis.granularity = 1
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: labelString)
        chartDataSet.colors = [UIColor(red: 230/255, green: 126/255, blue: 34/255, alpha: 1)]
        let chartData = BarChartData(dataSet: chartDataSet)
        chartData.setValueTextColor(UIColor.black)
        
        barChartView.leftAxis.axisMinimum = 0
        barChartView.rightAxis.axisMinimum = 0
        barChartView.xAxis.axisMinimum = 0
        barChartView.xAxis.labelFont = .systemFont(ofSize: 14)
        barChartView.data = chartData
        
        barChartView.xAxis.labelCount = 15
        
        barChartView.xAxis.labelPosition = .bottom
        let description = Description() // Description for BarChartView
        description.text = ""           // remove "No Description available" text
        barChartView.chartDescription = description
        barChartView.animate(yAxisDuration: 2.0) // move y Axis up.
    }
}

extension PedometerController: CLLocationManagerDelegate {
}
