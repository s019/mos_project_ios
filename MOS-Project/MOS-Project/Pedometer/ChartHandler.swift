//
//  ChartHandler.swift
//  MOS-Project
//
//  Created by Manuel Leibetseder on 21.11.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//

import Foundation
import CoreData

protocol ChartDelegate: class {
    func presentData(by amount: Int, and values: [Int])
}

class ChartHandler {

    var cdManager: CoreDataManager!
    weak var delegate: ChartDelegate?
    
    init() {
        self.cdManager  = CoreDataManager.sharedInstance
    }
    
    func presentToday() {
        // Get the current calendar with local time zone
        var calendar      = Calendar.current
        calendar.timeZone = NSTimeZone.system
        
        // Get today's beginning & end
        let dateFrom   = calendar.startOfDay(for: Date())
        var components = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: dateFrom)
        components.day! += 1
        let dateTo = calendar.date(from: components)!
  
        let predicate = Step.datePredicate(from: dateFrom, to: dateTo)

        self.cdManager.do(Step.request() as! NSFetchRequest<NSFetchRequestResult>, with: predicate) { [weak self] result in
            
            var stepsDay = Array(repeating: Int(0), count: .daysInCurrentMonth)
            (result as! [Step]).forEach { step in

                components = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: step.timestamp! as Date)
                stepsDay[components.hour!] += 1
            }
            
            self?.delegate?.presentData(by: 24, and: stepsDay)
        }
    }
    
    func presentWeek() {
        let predicate = Step.datePredicate(from: .startOfWeek, to: .endOfWeek)
        
        self.cdManager.do(Step.fetchRequest(), with: predicate) { [weak self] result in
            
            var stepsWeek = Array(repeating: Int(0), count: 7)
            (result as! [Step]).forEach { step in
            
                stepsWeek[step.weekday] += 1
            }

            self?.delegate?.presentData(by: 7, and: stepsWeek)
        }
    }
    
    func presentMonth() {
        // Get the current calendar with local time zone
        var calendar      = Calendar.current
        calendar.timeZone = NSTimeZone.system

        var dateFrom   = calendar.startOfDay(for: Date())
        var components = calendar.dateComponents([.year, .month], from: dateFrom)
        dateFrom = calendar.date(from: components)!
        components.month! += 1
        let dateTo = calendar.date(from: components)!
        
        let predicate = Step.datePredicate(from: dateFrom, to: dateTo)
        
        self.cdManager.do(Step.fetchRequest(), with: predicate) { [weak self] result in
            
            var stepsMonth = Array(repeating: Int(0), count: .daysInCurrentMonth)
            (result as! [Step]).forEach { step in
                components = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: step.timestamp! as Date)
                stepsMonth[components.day!] += 1
            }

            self?.delegate?.presentData(by: .daysInCurrentMonth, and: stepsMonth)
        }
    }
    
    func presentYear() {
        // Get the current calendar with local time zone
        var calendar      = Calendar.current
        calendar.timeZone = NSTimeZone.system
        
        // Get today's beginning & end
        var dateFrom   = calendar.startOfDay(for: Date())
        var components = calendar.dateComponents([.year], from: dateFrom)
        dateFrom = calendar.date(from: components)!
        components.year! += 1
        let dateTo = calendar.date(from: components)!
        
        let predicate = Step.datePredicate(from: dateFrom, to: dateTo)
        
        self.cdManager.do(Step.fetchRequest(), with: predicate) { [weak self] result in
            
            var stepsYear = Array(repeating: Int(0), count: 12)
            (result as! [Step]).forEach { step in
                var components = calendar.dateComponents([.year, .month], from: step.timestamp! as Date)
                stepsYear[components.month! - 1] += 1
            }
            
            self?.delegate?.presentData(by: 12, and: stepsYear)
        }
    }
}
