//
//  XAxisValueFormatter.swift
//  MOS-Project
//
//  Created by Gabriel Schützeneder on 20.11.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//

import Foundation
import Charts

public class XAxisValueFormatter: NSObject, IAxisValueFormatter {
    
    weak var chart: BarLineChartViewBase?

    var selected: Int
    var counter = 0
    
    init(chart: BarLineChartViewBase, selected: Int) {
        self.chart    = chart
        self.selected = selected
        self.counter  = 0
    }
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        
        if self.selected == 1 {
            
            let output   = Units.Days.prefixes[counter % 7]
            self.counter += 1
            return output
        } else if selected == 3 {
            
            let output   = Units.Months.prefixes[counter % 12]
            self.counter += 1
            return output
        } else {
            return "\(Int(value)) \n \(Units.Time.hour.description)"
        }
    }
}
