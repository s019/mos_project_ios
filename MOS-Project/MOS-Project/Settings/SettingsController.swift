//
//  SettingsController.swift
//  MOS-Project
//
//  Created by Gabriel Schützeneder on 05.12.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//

import UIKit
import Foundation

class SettingsController: UIViewController {
    
    @IBOutlet weak var labelAge: UILabel!
    @IBOutlet weak var textFieldAge: UITextField!
    @IBOutlet weak var labelGender: UILabel!
    @IBOutlet weak var segmentedControlGender: UISegmentedControl!
    @IBOutlet weak var labelWeight: UILabel!
    @IBOutlet weak var textFieldWeight: UITextField!
    @IBOutlet weak var labelRestingHeartrate: UILabel!
    @IBOutlet weak var textFieldHeartrate: UITextField!
    @IBOutlet weak var physicalRatingLabel: UILabel!
    @IBOutlet weak var textFieldPhysicalRating: UITextField!
    @IBOutlet weak var labelHeight: UILabel!
    @IBOutlet weak var textFieldHeight: UITextField!
    
    override func viewDidLoad() {
        setLabels()
        setTextFields()
        let settings = UserDefaults.standard.object(forKey: "settings") as! Bool
        if settings {
            restoreData()
        }
    }
    
    func setLabels() {
        labelAge.text = "Age"
        labelGender.text = "Gender"
        labelWeight.text = "Weight in kg"
        labelRestingHeartrate.text = "Resting Heartrate"
        physicalRatingLabel.text = "Physical Activity Rating"
        labelHeight.text = "Height in Meter"
    }
    
    func setTextFields() {
        textFieldAge.placeholder = "Age"
        textFieldAge.keyboardType = .numberPad
        textFieldWeight.placeholder = "kg"
        textFieldWeight.keyboardType = .numberPad
        textFieldHeartrate.placeholder = "e.g 60"
        textFieldHeartrate.keyboardType = .numberPad
        textFieldPhysicalRating.placeholder = "0 – 7"
        textFieldPhysicalRating.keyboardType = .numberPad
        textFieldHeight.placeholder = "1.7m"
        textFieldHeight.keyboardType = .decimalPad
    }
    
    func restoreData() {
        textFieldAge.text = UserDefaults.standard.string(forKey: "age")
        textFieldWeight.text = UserDefaults.standard.string(forKey: "weight")
        textFieldHeartrate.text = UserDefaults.standard.string(forKey: "restingHeartrate")
        let index = UserDefaults.standard.integer(forKey: "gender")
        segmentedControlGender.selectedSegmentIndex = index
        textFieldPhysicalRating.text = UserDefaults.standard.string(forKey: "physicalRating")
        textFieldHeight.text = UserDefaults.standard.string(forKey: "height")
    }

    @IBAction func saveContent(_ sender: Any) {
        
        UserDefaults.standard.set(true, forKey: "settings")
        UserDefaults.standard.set(textFieldAge.text, forKey: "age")
        UserDefaults.standard.set(segmentedControlGender.selectedSegmentIndex, forKey: "gender")
        UserDefaults.standard.set(textFieldWeight.text, forKey: "weight")
        UserDefaults.standard.set(textFieldHeartrate.text, forKey: "restingHeartrate")
        UserDefaults.standard.set(textFieldPhysicalRating.text, forKey: "physicalRating")
        
        var height = textFieldHeight.text
        height = String(format:"%.2f", height!.doubleValue)
        UserDefaults.standard.set(height, forKey: "height")
        self.tabBarController?.selectedIndex = 1
    }
    
    
}

extension String {
    static let numberFormatter = NumberFormatter()
    var doubleValue: Double {
        String.numberFormatter.decimalSeparator = "."
        if let result =  String.numberFormatter.number(from: self) {
            return result.doubleValue
        } else {
            String.numberFormatter.decimalSeparator = ","
            if let result = String.numberFormatter.number(from: self) {
                return result.doubleValue
            }
        }
        return 0
    }
}
