//
//  Enums.swift
//  MOS-Project
//
//  Created by Manuel Leibetseder on 21.11.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//

import Foundation

enum Units {
    
    enum Time: String, CustomStringConvertible {

        case hour = "Hour"
        
        var description: String {
            return self.rawValue
        }
    }
    
    enum Months: String, CustomStringConvertible {

        case january   = "January"
        case february  = "February"
        case march     = "March"
        case april     = "April"
        case may       = "May"
        case june      = "June"
        case july      = "July"
        case august    = "August"
        case september = "September"
        case october   = "October"
        case november  = "November"
        case december  = "Dezember"
        
        var description: String {
            return self.rawValue
        }
        
        var prefix: String {
            return String(self.rawValue.prefix(3))
        }
        
        static var prefixes: [String] {
            return [
                Units.Months.january.prefix,
                Units.Months.february.prefix,
                Units.Months.march.prefix,
                Units.Months.april.prefix,
                Units.Months.may.prefix,
                Units.Months.june.prefix,
                Units.Months.july.prefix,
                Units.Months.august.prefix,
                Units.Months.september.prefix,
                Units.Months.october.prefix,
                Units.Months.november.prefix,
                Units.Months.december.prefix
            ]
        }
    }

    enum Days: String, CustomStringConvertible {
        case sunday    = "Sunday"
        case monday    = "Monday"
        case tuesday   = "Tuesday"
        case wednesday = "Wednesday"
        case thursday  = "Thursday"
        case friday    = "Friday"
        case saturday  = "Saturday"
        
        var description: String {
            return self.rawValue
        }
        
        var prefix: String {
            return String(self.rawValue.prefix(3))
        }
        
        static var prefixes: [String] {
            return [
                Units.Days.sunday.prefix,
                Units.Days.monday.prefix,
                Units.Days.tuesday.prefix,
                Units.Days.wednesday.prefix,
                Units.Days.thursday.prefix,
                Units.Days.friday.prefix,
                Units.Days.saturday.prefix
            ]
        }
    }
}
