//
//  StepView.swift
//  MOS-Project
//
//  Created by Manuel Leibetseder on 21.11.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//

import Foundation

enum StepView: Int, RawRepresentable {
    typealias RawValue = Int
    
    case today = 0
    case week  = 1
    case month = 2
    case year  = 3

}
