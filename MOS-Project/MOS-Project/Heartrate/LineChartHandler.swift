//
//  LineChartHandler.swift
//  MOS-Project
//
//  Created by Gabriel Schützeneder on 06.12.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//

import Foundation
import Charts

class LineChartHandler {
    
    func getChartDataSet(values: [ChartDataEntry], label: String, r: CGFloat, g: CGFloat, b: CGFloat, textColor: UIColor) -> LineChartDataSet {
        let chartDataSet = LineChartDataSet(values: values, label: label)
        chartDataSet.colors = [UIColor(red: r, green: g, blue: b, alpha: 1)]
        return chartDataSet
    }
    
    func updateChart(chartView: LineChartView, sessions: [Session]) {
        chartView.noDataText = "No data available"
        var dataEntriesFatigue: [ChartDataEntry] = []
        var dataEntriesFitness: [ChartDataEntry] = []
        var dataEntriesPerformance: [ChartDataEntry] = []
        let amount = sessions.count
        
        for i in 0..<amount {
            let dataEntryFatigue = ChartDataEntry(x: Double(i), y: sessions[i].fatigue)
            let dataEntryFitness = ChartDataEntry(x: Double(i), y: sessions[i].fitness)
            let dataEntryPerformance = ChartDataEntry(x: Double(i), y: sessions[i].performance)
            dataEntriesFatigue.append(dataEntryFatigue)
            dataEntriesFitness.append(dataEntryFitness)
            dataEntriesPerformance.append(dataEntryPerformance)
        }
        let chartDataSetFatigue = getChartDataSet(values: dataEntriesFatigue, label: "Fatigue", r: 255, g: 0, b: 0, textColor: UIColor.black)
        let chartDataSetFitness = getChartDataSet(values: dataEntriesFitness, label: "Fitness", r: 0, g: 255, b: 0, textColor: UIColor.black)
        let chartDataSetPerformance = getChartDataSet(values: dataEntriesPerformance, label: "Performance", r: 0, g: 0, b: 255, textColor: UIColor.black)
        let chartData = LineChartData(dataSets: [chartDataSetFatigue, chartDataSetFitness, chartDataSetPerformance])
        chartData.setValueTextColor(UIColor.black)
        
        chartView.leftAxis.axisMinimum = 0
        chartView.rightAxis.axisMinimum = 0
        chartView.xAxis.axisMinimum = 0
        chartView.xAxis.labelFont = .systemFont(ofSize: 14)
        chartView.data = chartData
        
        chartView.xAxis.labelPosition = .bottom
        chartView.xAxis.labelFont = .systemFont(ofSize: 10)
        chartView.xAxis.granularity = 1
        chartView.xAxis.labelCount = 0
        
        chartView.xAxis.labelCount = 15
        
        chartView.xAxis.labelPosition = .bottom
        let description = Description() // Description for BarChartView
        description.text = ""           // remove "No Description available" text
        chartView.chartDescription = description
    }
}
