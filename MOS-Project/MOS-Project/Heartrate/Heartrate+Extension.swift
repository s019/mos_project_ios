//
//  Heartrate+Extension.swift
//  MOS-Project
//
//  Created by Gabriel Schützeneder on 06.12.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//

import Foundation
import CoreData
import HealthKit
import WatchConnectivity

extension HeartrateController: WCSessionDelegate {
    func sessionDidBecomeInactive(_ session: WCSession) {
        // stub
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        // stub
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        // stub
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String: Any]) {
        if let value = message["heartrate"] as? UInt16 {
            
            var heartrate = createHeartrateObj(value: Double(value))
            self.apiHandler.getHeartrateId { heartrateId in
                heartrate.id = heartrateId
                heartrate.sessionID = self.sessionId
                self.heartrateArray.append(heartrate)
                self.apiHandler.postHeartrate(heartrate: heartrate)
            }
            
            DispatchQueue.main.async {
                self.heartrateLabel.text = "Heartrate: \(value)"
                self.Math.isMin(value: Double(value))
                self.Math.isMax(value: Double(value))
                self.Math.getAvg(array: self.heartrateArray)
                self.Math.getPercHeartrate(value: Double(value))
                self.Math.calcKcal(curHeartrate: Double(value))
                self.updateLabels()
            }
        } else if let value = message["abortSession"] {
            print("\(value)")
            prepDataForSave()
            reset()
        } else if message["startSession"] != nil {
            startSession()
        }
    }
    
    func initResultController() -> NSFetchedResultsController<NSFetchRequestResult> {
        // Create Fetch Request
        let fetchRequest: NSFetchRequest = Step.fetchRequest()
        
        // Configure Fetch Request
        
        fetchRequest.sortDescriptors = [Step.nameDescriptor]
        
        // Create Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CoreDataManager.sharedInstance.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        
        // Configure Fetched Results Controller
        fetchedResultsController.delegate = self
        
        return fetchedResultsController as! NSFetchedResultsController<NSFetchRequestResult>
    }
}

extension HeartrateController: NSFetchedResultsControllerDelegate {
    
}
