//
//  HeartrateController.swift
//  MOS-Project
//
//  Created by Gabriel Schützeneder on 30.11.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//

import UIKit
import HealthKit
import WatchConnectivity
import Charts
import CoreData

class HeartrateController: UIViewController {

    @IBOutlet weak var heartrateLabel: UILabel!
    @IBOutlet weak var averageHeartrateLabel: UILabel!
    @IBOutlet weak var maximumHeartrateLabel: UILabel!
    @IBOutlet weak var minimumHeartrateLabel: UILabel!
    @IBOutlet weak var percMaxHeartrateLabel: UILabel!
    @IBOutlet weak var trimpHeartrateLabel: UILabel!
    
    @IBOutlet weak var chartView: LineChartView!
    
    var heartrateArray = [HeartrateObj]()
    var kcalArray = [Double]()
    var wcSession: WCSession!
    var apiHandler: APIHandler!
    var Math: HeartrateCalc!
    
    var startOfSession: NSDate!
    var endOfSession: NSDate!
    var sessionId: String!
    
    var lineChartHandler: LineChartHandler!
    
    var sessions: [Session] = [Session]()
    
    private var cdManager: CoreDataManager!
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchedResultsController = self.initResultController()
        self.lineChartHandler = LineChartHandler()
        self.cdManager = CoreDataManager.sharedInstance
        self.apiHandler = APIHandler()
        
        if WCSession.isSupported() {
            wcSession = WCSession.default
            wcSession.delegate = self
            wcSession.activate()
        }
        
        getSavedSessions()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let settings = UserDefaults.standard.object(forKey: "settings") as! Bool
        if !settings {
            // switch to settings
            self.tabBarController?.selectedIndex = 2
        } else {
            getSavedUserData()
        }
        lineChartHandler.updateChart(chartView: chartView, sessions: sessions)
    }
    
    func getSavedUserData() {
        let age = Int(UserDefaults.standard.string(forKey: "age")!)
        let weight = Int(UserDefaults.standard.string(forKey: "weight")!)
        let restingHeartrate = Int(UserDefaults.standard.string(forKey: "restingHeartrate")!)
        let gender = UserDefaults.standard.integer(forKey: "gender")
        let par = Int(UserDefaults.standard.string(forKey: "physicalRating")!)
        Math = HeartrateCalc(age: age!, weight: weight!, restingHeartrate: restingHeartrate!, gender: gender, par: par!)
    }
    
    func getSavedSessions() {
        let datePredicate = NSPredicate(format: "(start < %@)", argumentArray: [NSDate()])
        let sectionSortDescriptor = NSSortDescriptor(key: "start", ascending: false)
        let sortDescriptors = [sectionSortDescriptor]
        let moc = CoreDataManager.sharedInstance.viewContext // try to read data
        let fetchRequest: NSFetchRequest = Session.fetchRequest()
        fetchRequest.predicate = datePredicate
        fetchRequest.sortDescriptors = sortDescriptors
        do {
            sessions = try moc.fetch(fetchRequest)
        } catch {
            fatalError("Failed to fetch steps objects: \(error)")
        }
    }
    
    func saveSession(trimp: Double, days: Int, oldfitness: Double, fatigue: Double, fitness: Double) {
        
        self.cdManager.update { context in
            
            let session: Session = Session(context: context)
            session.id = self.sessionId
            session.start = self.startOfSession
            session.end = self.endOfSession
            for rate in self.heartrateArray {
                let heartrate: Heartrate = Heartrate(context: context)
                heartrate.timestamp = rate.timestamp
                heartrate.value = rate.value
                self.apiHandler.postHeartrate(heartrate: rate)
                session.addToHeartrate(heartrate)
            }
            session.trimp = trimp
            session.fatigue = fatigue
            session.fitness = fitness
            session.performance = self.Math.calcPerformance(fitness: fitness, fatigue: fatigue)
            let start = self.startOfSession as Date
            let minutes =  start.minutes(from: self.endOfSession as Date)
            session.kcal = self.Math.calcKcal(duration: minutes)
            
            self.apiHandler.post(session)
        }
        lineChartHandler.updateChart(chartView: chartView, sessions: sessions)
    }
    
    func reset() {
        
    }
    
    func prepDataForSave() {
        endOfSession = NSDate()
        
        let trimp = self.Math.calcTrimp(start: self.startOfSession, end: self.endOfSession)
        let daysSinceLastTraining = self.Math.daysSinceLastTraining(session: self.sessions.first!)
        let oldFitness = self.sessions.first!.fitness
        let fatigue = self.Math.calcFatigue(oldFitness: oldFitness, days: daysSinceLastTraining, trimp: trimp)
        let fitness = self.Math.calcFitness(oldFitness: oldFitness, days: daysSinceLastTraining, trimp: trimp)
        saveSession(trimp: trimp, days: daysSinceLastTraining, oldfitness: oldFitness, fatigue: fatigue, fitness: fitness)
    }
    
    func startSession() {
        self.startOfSession = NSDate()
        self.apiHandler.getSessionId { [weak self] sessionId in
            if let sessionId    = sessionId {
                self?.sessionId = sessionId
                print(self?.sessionId)
                UserDefaults.standard.set(sessionId, forKey: "sessionId")
            }
        }
    }
    
    func createHeartrateObj(value: Double) -> HeartrateObj {
        var heartrate = HeartrateObj()
        heartrate.value = value
        heartrate.timestamp = NSDate()
        return heartrate
    }
    
    func updateLabels() {
        self.percMaxHeartrateLabel.text = "\(Math.perc)"
        self.maximumHeartrateLabel.text = "\(Math.maximum)"
        self.minimumHeartrateLabel.text = "\(Math.minimum)"
        self.averageHeartrateLabel.text = "\(Math.average)"
        self.trimpHeartrateLabel.text = "\(String(describing: Math.kcalArray.last)) kcal/min"
    }
}
