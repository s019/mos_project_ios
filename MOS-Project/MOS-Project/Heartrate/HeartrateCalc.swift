//
//  HeartrateCalc.swift
//  MOS-Project
//
//  Created by Gabriel Schützeneder on 06.12.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//

import Foundation
import ObjectMapper

class HeartrateCalc {
    
    var maximum: Double = 0
    var minimum: Double = 250
    var average: Double = 0
    var perc: Int = 0
    
    var age: Int!
    var weight: Int!
    var restingHeartrate: Int!
    var gender: Int!
    var par: Int!
    
    var kcalArray = [Double]()
    
    init(age: Int, weight: Int, restingHeartrate: Int, gender: Int, par: Int) {
        self.age = age
        self.weight = weight
        self.restingHeartrate = restingHeartrate
        self.gender = gender
        self.par = par
    }
    
    func isMax(value: Double) {
        if value > maximum {
            maximum = value
        }
    }
    
    func isMin(value: Double) {
        if value < minimum {
            minimum = value
        }
    }
    
    func getAvg(array: [HeartrateObj]) {
        var avg = 0.0
        for heartrate in array {
            avg += heartrate.value
        }
        self.average = avg / Double(array.count)
    }
    
    func getPercHeartrate(value: Double) {
        let max = 211 - (0.64 * Double(age!))
        perc = Int((Double(value) / max) * 100)
    }
    
    // formula: 02_HeartRateMonitoring.pdf, page 32 & 33
    func calcTrimp(start: NSDate, end: NSDate) -> Double {
        var b: Double = 0.0
        if gender! == 0 {
            b = 1.92
        } else {
            b = 1.67
        }
        
        let diff: Date = start as Date
        let minutes = diff.minutes(from: end as Date)
        let numerator = average - Double(restingHeartrate!)   // Zähler
        let denominator = maximum - Double(restingHeartrate!) // Nenner
        let fraction = numerator / denominator
        let y = exp(fraction * b)
        
        let trimp = Double(minutes) * fraction * y
        print(trimp)
        return trimp
    }
    
    // formula: 03_WorkPhysiology.pdf, page 48
    func calcKcal(curHeartrate: Double) {
        var gender = 0.0
        if self.gender! == 0 {
            gender = 1
        } else {
            gender = 0
        }
        let age = -36.3781 + 0.271 * Double(self.age!)
        let weight = 0.394 * Double(self.weight!)
        let vomax = 0.404 * Double(self.par!)
        let heartrate = 0.634 * curHeartrate
        let firstSubResult = -59.3954 * gender * (age + weight + vomax + heartrate)
        
        let genderResult = 1 - gender
        let ageSubResult = 0.274 * Double(self.age!)
        let weightSubResult = 0.103 * Double(self.weight!)
        let vomaxSubResult = 0.380 * Double(self.par!)
        let heartrateSubResult = 0.450 * curHeartrate
        
        let secSubResult = genderResult * (ageSubResult + weightSubResult + vomaxSubResult + heartrateSubResult)
        var result = firstSubResult + secSubResult
        result /= 4168 //kJ in kcal
        kcalArray.append(result)
    }
    
    func calcKcal(duration minutes: Int) -> Double {
        let sumArray = kcalArray.reduce(0, +)
        let avg = sumArray / Double(kcalArray.count)
        return avg * Double(minutes)
    }
    
    func calcFitness(oldFitness: Double, days: Int, trimp: Double) -> Double {
        let result = oldFitness * exp(Double(-days/40)) * trimp
        return result
    }
    
    func calcFatigue(oldFitness: Double, days: Int, trimp: Double) -> Double {
        let result = oldFitness * exp(Double(-days/11)) * trimp
        return result
    }
    
    func calcPerformance(fitness: Double, fatigue: Double) -> Double {
        let result = 1 * fitness + 2 * fatigue
        return result
    }
    
    func daysSinceLastTraining(session: Session) -> Int {
        let start = session.start! as Date
        var days =  start.days(from: Date())
        days *= -1
        return days
    }
}

struct HeartrateObj {
    func encode() -> [String: Any] {
        let formatter = DateFormatter()
        formatter.dateFormat = APIStrings.iso8601Date.description
        let json = [
            "id": self.id!,
            "value": self.value,
            "timestamp": formatter.string(from: self.timestamp as Date),
            "sessionID": self.sessionID ?? ""
            ] as [String: Any]
        print(json)
        return json
    }
    
    var value: Double = 0
    var timestamp: NSDate!
    var id: String!
    var sessionID: String!
}
