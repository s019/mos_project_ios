//
//  Location+CoreDataClass.swift
//  MOS-Project
//
//  Created by Gabriel Schützeneder on 04.12.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Location)
public class Location: NSManagedObject, Encodable {

}

extension Location {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Location> {
        return NSFetchRequest<Location>(entityName: "Location")
    }
    
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var id: String?
    @NSManaged public var step: Step?
    
}
