//
//  Step+CoreDataClass.swift
//  MOS-Project
//
//  Created by Gabriel Schützeneder on 04.12.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Step)
public class Step: NSManagedObject {
    static let nameDescriptor: NSSortDescriptor = NSSortDescriptor(key: StepEnum.timestamp.description, ascending: true)
}

extension Step {
    
    static func datePredicate(from: Date, to: Date) -> NSPredicate {
        return NSPredicate(format: "(%@ <= timestamp) AND (timestamp < %@)", argumentArray: [from, to])
    }
    
    static func request() -> NSFetchRequest<Step> {
        
        return NSFetchRequest<Step>(entityName: StepEnum.step.description)
    }
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Step> {
        
        return NSFetchRequest<Step>(entityName: StepEnum.step.description)
    }

    @NSManaged public var id: String?
    @NSManaged public var timestamp: NSDate?
    @NSManaged public var location: Location?
    @NSManaged public var session: Session?
    
    var weekday: Int {
        var calendar      = Calendar.current
        calendar.timeZone = NSTimeZone.system
        
        var components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .weekday], from: self.timestamp! as Date)
        return components.weekday! - 1
    }
}

enum StepEnum: String, CustomStringConvertible {
    
    case step      = "Step"
    case timestamp = "timestamp "
    
    var description: String {
        return self.rawValue
    }
}
