//
//  Heartrate+CoreDataClass.swift
//  MOS-Project
//
//  Created by Gabriel Schützeneder on 04.12.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Heartrate)
public class Heartrate: NSManagedObject, Encodable {
    
}

extension Heartrate {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Heartrate> {
        return NSFetchRequest<Heartrate>(entityName: "Heartrate")
    }
    
    @NSManaged public var value: Double
    @NSManaged public var timestamp: NSDate?
    @NSManaged public var id: String?
    @NSManaged public var session: Session?
    
}

extension Heartrate {
    func encode(with sessionID: String) -> [String: Any] {
        let formatter = DateFormatter()
        formatter.dateFormat = APIStrings.iso8601Date.description
        
        return [ "heartrate": [
            "id": self.id!,
            "timestamp": formatter.string(from: self.timestamp! as Date),
            "session_id": sessionID,
            "value": self.value
            ]]
    }
}
