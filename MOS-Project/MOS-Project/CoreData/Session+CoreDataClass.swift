//
//  Session+CoreDataClass.swift
//  MOS-Project
//
//  Created by Gabriel Schützeneder on 05.12.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Session)
public class Session: NSManagedObject, Encodable {
    
}

extension Session {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Session> {
        return NSFetchRequest<Session>(entityName: "Session")
    }
    
    @NSManaged public var end: NSDate?
    @NSManaged public var id: String?
    @NSManaged public var kcal: Double
    @NSManaged public var start: NSDate?
    @NSManaged public var trimp: Double
    @NSManaged public var performance: Double
    @NSManaged public var fatigue: Double
    @NSManaged public var fitness: Double
    @NSManaged public var heartrate: NSSet?
    @NSManaged public var step: NSSet?
    
}

// MARK: Generated accessors for heartrate
extension Session {
    
    @objc(addHeartrateObject:)
    @NSManaged public func addToHeartrate(_ value: Heartrate)
    
    @objc(removeHeartrateObject:)
    @NSManaged public func removeFromHeartrate(_ value: Heartrate)
    
    @objc(addHeartrate:)
    @NSManaged public func addToHeartrate(_ values: NSSet)
    
    @objc(removeHeartrate:)
    @NSManaged public func removeFromHeartrate(_ values: NSSet)
    
}

// MARK: Generated accessors for step
extension Session {
    
    @objc(addStepObject:)
    @NSManaged public func addToStep(_ value: Step)
    
    @objc(removeStepObject:)
    @NSManaged public func removeFromStep(_ value: Step)
    
    @objc(addStep:)
    @NSManaged public func addToStep(_ values: NSSet)
    
    @objc(removeStep:)
    @NSManaged public func removeFromStep(_ values: NSSet)
    
}

extension Session {
    func encode() -> [String: Any] {
        let formatter = DateFormatter()
        formatter.dateFormat = APIStrings.iso8601Date.description
        
        return [
            "id": self.id!,
            "start": formatter.string(from: self.start! as Date),
            "end": formatter.string(from: self.end! as Date),
            "fatigue": self.fatigue,
            "trimp": self.trimp,
            "fitness": self.fitness,
            "kcal": self.kcal,
            "performance": self.performance
        ]
    }
}
