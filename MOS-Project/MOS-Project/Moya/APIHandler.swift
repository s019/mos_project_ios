//
//  APIHandler.swift
//  MOS-Project
//
//  Created by Manuel Leibetseder on 22.11.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//

import Foundation
import RxSwift
import Moya

class APIHandler {
    
    let provider: MoyaProvider<API>!
    
    init() {
        self.provider = MoyaProvider<API>()
    }
    
    func post(date: Date, _ closure: @escaping (_ id: String?, _ date: Date) -> Void) {
        
        print("Requesting ID for step")
        _ = self.provider.rx.request(.POST_DATE(date: date)).subscribe { event in
            switch event {
            case let .success(response):
                let resp = ResponseID.decode(response.data)
                closure(resp?.id, date)
            case let .error(error):
                print(error)
            }
        }
    }
    
    func getSessionId(_ closure: @escaping (_ id: String?) -> Void) {
        
        _ = self.provider.rx.request(.GET_SESSION_ID).subscribe { event in
            switch event {
            case let .success(response):
                let resp = ResponseID.decode(response.data)
                closure(resp?.id)
            case let .error(error):
                print(error)
            }
        }
    }
    
    func getHeartrateId(_ closure: @escaping (_ id: String?) -> Void) {
        
        _ = self.provider.rx.request(.GET_HEARTRATE_ID).subscribe { event in
            switch event {
            case let .success(response):
                let resp = ResponseID.decode(response.data)
                closure(resp?.id)
            case let .error(error):
                print(error)
            }
        }
    }
    
    func postHeartrate(heartrate: HeartrateObj) {
        _ = self.provider.rx.request(.POST_HEARTRATE(heartrate: heartrate)).subscribe { event in
            switch event {
            case let .success(response):
                print(String(data: response.data, encoding: .utf8)!)
            case let .error(error):
                print(error)
            }
        }
    }
    
    func post(_ session: Session) {
        _ = self.provider.rx.request(.POST_SESSION(session: session)).subscribe { event in
            switch event {
            case let .success(response):
                print(String(data: response.data, encoding: .utf8)!)
            case let .error(error):
                print(error)
            }
        }
    }
}
