//
//  API.swift
//  
//  Generated usign MoyaPaw Export https://github.com/narlei/SwiftMoyaCodeGenerator.git

import Moya

enum API {
    case POST_DATE(date: Date)
    case GET_SESSION_ID
    case GET_HEARTRATE_ID
    case POST_HEARTRATE(heartrate: HeartrateObj)
    case POST_SESSION(session: Session)
}

extension API: TargetType {

    var baseURL: URL { return URL(string: APIStrings.url.description)! }

    var path: String {
        switch self {
        case .POST_DATE(_):
            return APIStrings.postDateURI.description
        case .GET_SESSION_ID:
            return APIStrings.getSessionIdURI.description
        case .GET_HEARTRATE_ID:
            return APIStrings.getHeartrateIdURI.description
        case .POST_HEARTRATE(let heartrate):
            return "\(APIStrings.postHeartrateURI.description)/\(heartrate.id!)"
        case .POST_SESSION(let session):
            return "\(APIStrings.postSessionURI.description)/\(session.id!)"
        }
    }

    var method: Moya.Method {
        switch self {
        case .POST_DATE, .POST_HEARTRATE, .POST_SESSION:
            return .post
        case .GET_SESSION_ID, .GET_HEARTRATE_ID:
            return .get
        }
    }

    var task: Task {
        switch self {
        case .POST_DATE(let date):
            let formatter        = DateFormatter()
            formatter.dateFormat = APIStrings.iso8601Date.description

            return .requestParameters(parameters: [APIStrings.jsonPOSTKey.description: formatter.string(from: date)], encoding: JSONEncoding.default)
        case .POST_HEARTRATE(let heartrate):
            return .requestParameters(parameters: heartrate.encode(), encoding: JSONEncoding.default)
        case .POST_SESSION(let session):
            return .requestParameters(parameters: session.encode(), encoding: JSONEncoding.default)
        default:
            return .requestParameters(parameters: [:], encoding: URLEncoding.default)
        }
    }

    var headers: [String: String]? {
        return [APIStrings.httpContentType.description: APIStrings.httpApplicationJSON.description ]
    }

    var sampleData: Data {
        switch self {
        case .POST_DATE, .GET_SESSION_ID,
             .GET_HEARTRATE_ID, .POST_HEARTRATE,
             .POST_SESSION:
            return "{}".data(using: String.Encoding.utf8)!
        }
    }
    
    var parameterEncoding: Moya.ParameterEncoding {
        return JSONEncoding.default
    }
}

extension String {

    var urlEncoded: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return self.data(using: .utf8)!
    }
}
