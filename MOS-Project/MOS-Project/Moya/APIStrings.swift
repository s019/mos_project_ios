//
//  Format.swift
//  MOS-Project
//
//  Created by Manuel Leibetseder on 22.11.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//

import Foundation

enum APIStrings: String, CustomStringConvertible {

    case iso8601Date         = "yyyy-MM-dd'T'HH:mm:ssZ"
    case httpContentType     = "Content-Type"
    case httpApplicationJSON = "application/json"
    case jsonPOSTKey         = "content"
    case url                 = "http://192.168.1.46:8080"
    case postDateURI         = "/steps/date"
    case getSessionIdURI     = "/sessions/id"
    case getHeartrateIdURI   = "/heartrates/id"
    case postHeartrateURI    = "/heartrates"
    case postSessionURI      = "/sessions"
    
    var description: String {
        return self.rawValue
    }
}
