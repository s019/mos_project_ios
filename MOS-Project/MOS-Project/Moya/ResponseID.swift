//
//  JSONer.swift
//  MOS-Project
//
//  Created by Manuel Leibetseder on 22.11.17.
//  Copyright © 2017 Gabriel Schützeneder. All rights reserved.
//

import Foundation

struct ResponseID: Codable {
    let id: String?
    
    enum CodingKeys: String, CodingKey {
        case id
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
    }
    
    static func decode(_ data: Data) -> ResponseID? {
        do {
            let decoded = try JSONDecoder().decode([ResponseID].self, from: data)
            return decoded.first
        } catch {
            print(error)
            return nil
        }
    }
}
